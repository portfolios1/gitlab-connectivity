import { BadgeDataproviderService } from "../services/badge.dataprovider.service";
import { IBadge } from "../domainEntities/badge.i";

export class BadgeModel {
    private _badges: IBadge[];
    constructor(private badgeProvider: BadgeDataproviderService) {}

    public get badges(): IBadge[] {
        return this._badges;
    }

    public async init() {
        await this.badgeProvider.start();
        this._badges = this.badgeProvider.badges;
    }
}
