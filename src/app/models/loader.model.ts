import { LoaderService } from "../services/loader.service";

export class LoaderModel {
    private _loaderState: boolean;
    constructor(private loaderService: LoaderService) {
        this.loaderService.onLoaderStateChanged.subscribe(
            this.onLoaderStateChanged,
            this
        );
    }

    public get loaderState(): boolean {
        return this._loaderState;
    }

    public onLoaderStateChanged(sender: any) {
        console.log("here");
        this._loaderState = this.loaderService.loaderState;
    }
}
