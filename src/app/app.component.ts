import { Component, OnInit, AfterViewInit } from "@angular/core";
import { BadgeDataproviderService } from "./services/badge.dataprovider.service";
import { BadgeModel } from "./models/badge.model";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit, AfterViewInit {
    public title = "gitlab-connectivity";
    constructor(private badgeProvider: BadgeDataproviderService) {
        this.model = new BadgeModel(badgeProvider);
    }

    public readonly model: BadgeModel;
    public ngOnInit() {
        this.model.init();
    }
    public ngAfterViewInit() {
        window.parent.postMessage(`${document.body.scrollHeight}px`, "*");
    }
}
