export interface IProject {
    id: number;
    name: string;
    description: string;
}

export interface IProjectContract {
    id: number;
    name: string;
    description: string;
    web_url: string;
}

export class Project implements IProject {
    constructor(project: IProjectContract) {
        this.id = project.id;
        this.name = project.name;
        this.description = project.description;
        this.webUrl = project.web_url;
    }

    public readonly id: number;
    public readonly name: string;
    public readonly description: string;
    public readonly webUrl: string;
}
