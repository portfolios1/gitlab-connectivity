export interface IGroup {
    id: string;
    description: string;
    defaultBranch: string;
    name: string;
}

export interface IGroupContract {
    id: string;
    description: string;
    default_branch: string;
    name: string;
}

export class Group implements IGroup {
    constructor(group: IGroupContract) {
        this.id = group.id;
        this.description = group.description;
        this.name = group.name;
        this.defaultBranch = group.default_branch;
    }
    public id: string;
    public description: string;
    public defaultBranch: string;
    public name: string;
}
