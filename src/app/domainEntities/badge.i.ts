export interface IBadge {
    id: number;
    name: string;
    linkUrl: string;
    imageUrl: string;
}

export interface IBadgeContract {
    id: number;
    name: string;
    link_url: string;
    image_url: string;
    rendered_link_url: string;
    rendered_image_url: string;
}

export class Badge implements IBadge {
    constructor(badge: IBadgeContract) {
        this.id = badge.id;
        this.name = badge.name;
        this.linkUrl = badge.link_url;
        this.imageUrl = badge.image_url;
    }
    public readonly id: number;
    public readonly name: string;
    public readonly linkUrl: string;
    public readonly imageUrl: string;
}
