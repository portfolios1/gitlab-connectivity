import { Component, OnInit } from "@angular/core";

import { LoaderService } from "../../services/loader.service";
import { LoaderModel } from "src/app/models/loader.model";

@Component({
    selector: "app-loader",
    templateUrl: "./loader.component.html"
})
export class LoaderComponent implements OnInit {
    constructor(private loaderService: LoaderService) {
        this.model = new LoaderModel(loaderService);
    }

    public model: LoaderModel;
    ngOnInit() {}
}
