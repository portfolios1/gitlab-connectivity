import { TestBed } from "@angular/core/testing";

import { GroupRepository } from "./group.repository";

describe("GroupsService", () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it("should be created", () => {
        const service: GroupRepository = TestBed.get(GroupRepository);
        expect(service).toBeTruthy();
    });
});
