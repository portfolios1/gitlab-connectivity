import { TestBed } from "@angular/core/testing";

import { ProjectRepository } from "./project.repository";

describe("ProjectsService", () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it("should be created", () => {
        const service: ProjectRepository = TestBed.get(ProjectRepository);
        expect(service).toBeTruthy();
    });
});
