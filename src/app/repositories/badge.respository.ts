import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

import { BaseUrl, Token, BadgeType } from "../app.constants";
import { IBadge, IBadgeContract, Badge } from "../domainEntities/badge.i";
import { IBadgeRepository } from "./repository.i";
import { ProjectRepository } from "./project.repository";

@Injectable({
    providedIn: "root"
})
export class BadgeRepository implements IBadgeRepository {
    constructor(
        private http: HttpClient,
        private projectRepository: ProjectRepository
    ) {}

    public async getAllBadges() {
        const projects = await this.projectRepository.getAllProjects();
        console.log(projects);
        projects.forEach(async project => {
            const badges = await this.getBadges(project.id);
            badges &&
                badges.forEach(badge => {
                    if (badge.name === BadgeType) console.log(badge);
                });
        });
    }

    public async getBadges(projectId: number): Promise<IBadge[]> {
        return this.http
            .get<IBadgeContract[]>(
                `${BaseUrl}projects/${projectId}/badges?private_token=${Token}`
            )
            .pipe(
                map<IBadgeContract[], IBadge[]>(result =>
                    result.map(badge => new Badge(badge))
                )
            )
            .toPromise();
    }
}
