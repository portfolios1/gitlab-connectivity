import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

import { ProjectUrl } from "../app.constants";
import {
    IProject,
    Project,
    IProjectContract
} from "../domainEntities/project.i";
import { IProjectsRepository } from "./repository.i";

@Injectable({
    providedIn: "root"
})
export class ProjectRepository implements IProjectsRepository {
    constructor(private http: HttpClient) {}

    public async getAllProjects(): Promise<IProject[]> {
        return this.http
            .get<IProjectContract[]>(ProjectUrl)
            .pipe(
                map<IProjectContract[], IProject[]>(result =>
                    result.map(project => new Project(project))
                )
            )
            .toPromise();
    }
}
