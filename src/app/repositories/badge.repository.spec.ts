import { TestBed } from "@angular/core/testing";

import { BadgeRepository } from "./badge.respository";

describe("BadgeRepository", () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it("should be created", () => {
        const service: BadgeRepository = TestBed.get(BadgeRepository);
        expect(service).toBeTruthy();
    });
});
