import { IProject } from "../domainEntities/project.i";
import { IBadge } from "../domainEntities/badge.i";

export interface IBadgeRepository {
    getBadges(projectId: number): Promise<IBadge[]>;
    getAllBadges();
}

export interface IProjectsRepository {
    getAllProjects(): Promise<IProject[]>;
}
