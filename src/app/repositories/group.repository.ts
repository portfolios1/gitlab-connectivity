import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { map } from "rxjs/operators";
import { IGroup, IGroupContract, Group } from "../domainEntities/group.i";
import { GroupsUrl } from "../app.constants";
import {
    IProjectContract,
    IProject,
    Project
} from "../domainEntities/project.i";

@Injectable({
    providedIn: "root"
})
export class GroupRepository {
    constructor(private http: HttpClient) {}

    public async getGroups(): Promise<IGroup[]> {
        return this.http
            .get<IGroupContract[]>(GroupsUrl)
            .pipe(
                map<IGroupContract[], IGroup[]>(result =>
                    result.map(group => new Group(group))
                )
            )
            .toPromise();
    }

    public async getGroupProjects(id: string): Promise<IProject[]> {
        return this.http
            .get<IProjectContract[]>(`${GroupsUrl}/${id}/projects`)
            .pipe(
                map<IProjectContract[], IProject[]>(result =>
                    result.map(project => new Project(project))
                )
            )
            .toPromise();
    }
}
