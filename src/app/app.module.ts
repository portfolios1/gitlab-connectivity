import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { httpInterceptorProviders } from "./infra/httpInterceptor/index";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BadgeModule } from "./components/badge/badge.module";
import { LoaderComponent } from "./components/loader/loader.component";

@NgModule({
    declarations: [AppComponent, LoaderComponent],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule, BadgeModule],
    providers: [httpInterceptorProviders],
    bootstrap: [AppComponent]
})
export class AppModule {}
