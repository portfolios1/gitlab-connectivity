import { Observable } from "rxjs";

export interface IAsyncService<T> {
    isReady: boolean;
    ready: Observable<T>;
    setBusy(): void;
    setFree(): void;
}

export class AsyncServiceBase<T> implements IAsyncService<T> {
    private _isReady: boolean;
    constructor() {
        this._isReady = false;
    }
    public readonly ready: Observable<T>;

    public get isReady(): boolean {
        return this._isReady;
    }

    public setBusy(): void {
        this._isReady = false;
    }

    public setFree(): void {
        this._isReady = true;
    }

    public registerAsyncObjects() {}
}
