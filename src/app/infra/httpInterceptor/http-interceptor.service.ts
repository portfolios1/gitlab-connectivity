import { Injectable } from "@angular/core";
import {
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpEvent
} from "@angular/common/http";
import { Observable } from "rxjs";
import { Token } from "src/app/app.constants";

@Injectable({
    providedIn: "root"
})
export class HttpInterceptorService implements HttpInterceptor {
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const request = req.clone({
            headers: req.headers.set("PRIVATE-TOKEN", Token)
        });
        return next.handle(request);
    }
}
