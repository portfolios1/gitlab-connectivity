export interface IEvent {
    subscribe: (subscriber: (sender: any) => void, context: any) => void;
    unsubscribe: (subscriber: (sender: any) => void) => void;
}

export class Event {
    private _subscribers: any[];
    constructor() {
        this._subscribers = [];
    }

    public subscribe(subscriber: any, context: any) {
        this._subscribers.push(subscriber.bind(context));
    }

    public raise(sender: any) {
        this._subscribers.forEach(subscriber => subscriber(sender));
    }

    public unsubscribe(subscriber) {
        const indexOf = this._subscribers.indexOf(subscriber);

        if (indexOf > -1) {
            this._subscribers.splice(indexOf, 1);
        }
    }
}
