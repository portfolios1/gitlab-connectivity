import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AsyncServiceBase } from "../infra/Service.i";
import { IBadgeDataProviderService } from "./service.i";
import { IBadge } from "../domainEntities/badge.i";
import { BadgeRepository } from "../repositories/badge.respository";
import { LoaderService } from "./loader.service";
import { LoggerService } from "./logger.service";
import { GroupRepository } from "../repositories/group.repository";
import { ProjectRepository } from "../repositories/project.repository";

@Injectable({
    providedIn: "root"
})
export class BadgeDataproviderService extends AsyncServiceBase<void>
    implements IBadgeDataProviderService<void> {
    private static BADGE_TYPE = "Portfolio";
    private _badges: IBadge[] = [];
    constructor(
        private badgeRepository: BadgeRepository,
        private groupRepository: GroupRepository,
        private loaderService: LoaderService,
        private loggerService: LoggerService,
        private projectRepository: ProjectRepository
    ) {
        super();
    }

    public get badges(): IBadge[] {
        return Object.values(this._badges);
    }

    public async start(): Promise<void> {
        this.loaderService.showLoader();
        const groups = await this.groupRepository.getGroups();
        await Promise.all(
            groups.map(async group => {
                const projects = await this.groupRepository.getGroupProjects(
                    group.id
                );
                await Promise.all(
                    projects.map(async project => {
                        const badges = await this.badgeRepository.getBadges(
                            project.id
                        );
                        this.processBadges(badges);
                    })
                );
            })
        )
            .then(() => this.loaderService.hideLoader())
            .catch(err => this.loggerService.showLog(err));
        // const projects = await this.projectRepository.getAllProjects();
        // console.log(projects);
        // await Promise.all(
        //     projects.map(async project => {
        //         const badges = await this.badgeRepository.getBadges(project.id);
        //         console.log(badges);
        //         this.processBadges(badges);
        //     })
        // )
        //     .then(() => {
        //         this.loaderService.hideLoader();
        //     })
        //     .catch(error => {
        //         this.loggerService.showLog(error);
        //     });
    }

    private processBadges(badges: IBadge[]) {
        if (!badges) return;

        badges.forEach(badge => {
            if (badge.name === BadgeDataproviderService.BADGE_TYPE) {
                this._badges.push(badge);
            }
        });
    }
}
