import { TestBed } from "@angular/core/testing";

import { BadgeDataproviderService } from "./badge.dataprovider.service";

describe("Badge.DataproviderService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: BadgeDataproviderService = TestBed.get(
      BadgeDataproviderService
    );
    expect(service).toBeTruthy();
  });
});
