import { Injectable } from "@angular/core";
import { Event, IEvent } from "../infra/Event.i";

@Injectable({
    providedIn: "root"
})
export class LoaderService {
    private _loaderState: boolean;
    private _onLoaderStateChanged: Event;
    constructor() {
        this._loaderState = false;
        this._onLoaderStateChanged = new Event();
    }

    public get loaderState(): boolean {
        return this._loaderState;
    }

    public get onLoaderStateChanged(): IEvent {
        return this._onLoaderStateChanged;
    }

    public showLoader() {
        this._loaderState = true;
        this._onLoaderStateChanged.raise(this);
    }

    public hideLoader() {
        this._loaderState = false;
        this._onLoaderStateChanged.raise(this);
    }
}
