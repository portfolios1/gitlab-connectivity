import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class LoggerService {
    public showLog(error: any) {
        console.log(error);
    }
}
