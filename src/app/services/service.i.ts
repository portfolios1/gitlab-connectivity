import { IAsyncService } from "../infra/Service.i";
import { IBadge } from "../domainEntities/badge.i";

export interface IBadgeDataProviderService<T> extends IAsyncService<void> {
    badges: IBadge[];
    start(): Promise<T>;
}
